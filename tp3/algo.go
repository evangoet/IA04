package main


func removeFromSlice(slice []Agent, s int) []Agent {
    return append(slice[:s], slice[s+1:]...)
}

func removeFromSliceID(slice []AgentID, s int) []AgentID{
  slice[s]=slice[len(slice)-1]
  return slice[:len(slice)-1]
}

func Init_pref(pool []Agent) (result map[AgentID][]AgentID) {
	//Fonction renvoyant un map associant à un agentID (clé) le tableau des préférences de l'agent correspondant (valeur)
	result = make(map[AgentID][]AgentID)
	for _,v := range pool {
			result[v.ID]=v.Prefs
	}
	return
}


func propositionRecu(disposant []Agent,proposant []Agent, level int) map[AgentID][]Agent {
	var proposition = make(map[AgentID][]Agent)
	for _,x := range disposant{
		for _, id := range proposant{
			if id.Prefs[level] == x.ID {
				proposition[x.ID]= append(proposition[x.ID],id)
			}
		}
	}
	return proposition
}

func findBestPref(disposant Agent, proposant []Agent) Agent{
	Pref := proposant[0]
	for i:=1; i< len(proposant); i++{
		Flag,_ := disposant.Prefers(proposant[i], Pref)
		if Flag{
			Pref = proposant[i]
		}
	}
	return Pref
}

func findAgentFromId(list []Agent, id AgentID ) Agent{
	var agent Agent
	for _,x := range list{
		if x.ID == id {
			agent = x
		}
	}
	return agent
}

func cloneSlice(slc1 []Agent)(slc2 []Agent){
	slc2 = make([]Agent, len(slc1))
	copy(slc2,slc1)
	return
}

func boston(proposant []Agent, disposant []Agent) map[AgentID]AgentID{
	var unadmitted []Agent //list des proposant pas admis
	var couple = make(map[AgentID]AgentID)
	var level = 0
	var disposant_unadmitted []Agent

	//initialisation des non proposant admis TODO : faire fonction initUnadmitted
	for x:=0; x<len(proposant); x++{
		unadmitted= append(unadmitted, proposant[x])
	}
	//initialisation des disposant non admis TOD0 : faire fonction initUnadmitted
	for y :=0; y<len(disposant);y++{
		disposant_unadmitted = append(disposant_unadmitted, disposant[y])
	}

	//map des propositions
	var proposition = make(map[AgentID][]Agent)

	for len(unadmitted) > 0 {
		//ont prends la liste des proposition en fonction du level
		proposition = propositionRecu(disposant_unadmitted, unadmitted, level)
		for key, element := range proposition {
			keyID := findAgentFromId(disposant_unadmitted, key)
			Pref := findBestPref(keyID, element) //TODO:verifier qu'il existe
			couple[key]= Pref.ID
		}

		//MAJ du nouveau groupe des unadmitted TODO : faire fonction deleteElementFromSlice
		for _,prop := range couple{
			for i:=0; i < len(unadmitted);i++{
				if unadmitted[i].ID == prop {
					unadmitted = removeFromSlice(unadmitted, i)
					i--
				}
			}
		}

		//TODO : faire fonction deleteElementFromSlice
		for disp,_ := range couple {
			for i:=0;i<len(disposant_unadmitted);i++{
				if disposant_unadmitted[i].ID == disp {
					//Supprimer
					disposant_unadmitted = removeFromSlice(disposant_unadmitted, i)
					i--
				}
			}

		}
		level +=1
	}
	return couple
}

/*func boston(proposant []Agent, disposant []Agent) map[AgentID]AgentID{
	var unadmitted =make(map[int][]Agent) //list des proposant pas admis
	var couple = make(map[AgentID]AgentID)
	var level = 0
	var disposant_unadmitted = make(map[int][]Agent)

	//initialisation des non proposant admis
	for x:=0; x<len(proposant); x++{
		unadmitted[0]= append(unadmitted[0], proposant[x])
	}
	//initialisation des disposant non admis
	for y :=0; y<len(disposant);y++{
		disposant_unadmitted[0] = append(disposant_unadmitted[0], disposant[y])
	}

	//map des propositions
	var proposition = make(map[AgentID][]Agent)

	for len(unadmitted[level]) > 0 {
		//ont prends la liste des proposition en fonction du level
		proposition = propositionRecu(disposant_unadmitted[level], unadmitted[level], level)
		for key, element := range proposition {
			keyID := findAgentFromId(disposant_unadmitted[level], key)
			Pref := findBestPref(keyID, element) //TODO:verifier qu'il existe
			couple[key]= Pref.ID
		}

		//MAJ du nouveau groupe des unadmitted
		temp := unadmitted[level]
		var nb1 = 0
		for i:=0; i< len(unadmitted); i ++ {
			for _,prop := range couple{
				if i.ID == prop {
					temp = removeFromSlice(temp, pos - nb1)
					nb1+=1
				}
			}
		}
		unadmitted[level+1] = temp
		fmt.Println(couple)
		fmt.Println(unadmitted[level+1])

		/*for pos,i := range disposant_unadmitted[level] {
			for disp,_ := range couple{
				if i.ID == disp {
					//Supprimer l'élement
					disposant_unadmitted[level+1] = removeFromSlice(disposant_unadmitted[level], pos)
				}
			}

		}
		level +=1
	}
	return couple
}*/
