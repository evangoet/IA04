package main



func DeleteRestant(liste []AgentID, element AgentID) (result []AgentID) {
	//Fonction supprimant un ID précis d'une liste d'ID
	for i := range liste {
		if liste[i]==element {

      result = removeFromSliceID(liste, i)
		}
	}

	return
}

func IsIn(sl []AgentID, id AgentID) bool{
	//Fonction renvoyant un booléen selon la présence d'un ID dans un tableau d'ID
	for _,v := range sl {
		if v==id {
			return true
		}
	}
	return false
}

func BuildCycle(prop_restant []AgentID, disp_restant []AgentID, prop []Agent, disp []Agent) []AgentID {
	var cycle []AgentID
	cur_id := prop_restant[0]
	for IsIn(cycle, cur_id)==false {
		p:=SearchPref(cur_id, disp_restant, prop)
		cycle=append(cycle,cur_id)
		cur_id=SearchPref(p, prop_restant, disp)
		cycle=append(cycle,p)
	}
	return cycle
}

func SearchPref(id AgentID, restant []AgentID, pool []Agent) AgentID {
	ag := findAgentFromId(pool, id)
	for _,v := range ag.Prefs {
		if IsIn(restant, v) {
			return v
		}
	}
	return ""
}

func TTC(prop []Agent, disp []Agent) (appariement map[AgentID]AgentID) {
	appariement = make(map[AgentID]AgentID)
  var prop_restant []AgentID
  for _,v := range prop {
			prop_restant=append(prop_restant,v.ID)
	}

  var disp_restant []AgentID
  for _,v := range disp {
			disp_restant=append(disp_restant,v.ID)
	}
	for len(prop_restant) > 0 {
		cycle := BuildCycle(prop_restant,disp_restant,prop,disp)
		for len(cycle)!=0 && appariement[cycle[len(cycle)-1]]=="" {
			appariement[cycle[len(cycle)-1]]=cycle[len(cycle)-2]
			disp_restant=DeleteRestant(disp_restant, cycle[len(cycle)-1])
			prop_restant=DeleteRestant(prop_restant, cycle[len(cycle)-2])
			cycle=cycle[:len(cycle)-2]
		}
	}
	return
}

/*var unadmitted []AgentID

func is_free(a Agent) bool {
	for _, id := range unadmitted {
		if id == a.ID {
			return true
		}
	}

	return false
}

func DeleteAgent(liste []Agent, element AgentID) (result []Agent) {
	//Fonction supprimant un ID précis d'une liste d'ID
	for i,agent := range liste {
    fmt.Println(i)
    fmt.Println(agent)
		if agent.ID==element {
			liste= removeFromSlice(liste, i)
		}
	}
	//result=liste[:len(liste)-1]
	return
}

func (a AgentID) findFPrefFree(list []Agent) AgentID {
  agent :=findAgentFromId(list, a)
	for _, id := range agent.Prefs {
		for _, id_free := range unadmitted {
			if id == id_free {
				return id
			}
		}
	}

	return ""
}

func isIn(sl []Agent, id Agent) bool{
	//Fonction renvoyant un booléen selon la présence d'un ID dans un tableau d'ID
	for _,v := range sl {
		if v.ID==id.ID {
			return true
		}
	}
	return false
}


func BuildCycle(prop_restant []Agent, prop []Agent, disp []Agent) []Agent {

	var cycle []Agent
  cur_agent := prop_restant[0]
	cur_id := prop_restant[0].ID
	for isIn(cycle, cur_agent)==false {
		pref :=cur_id.findFPrefFree(prop_restant)
    cur_agent = findAgentFromId(prop_restant, cur_id)
		cycle=append(cycle,cur_agent)
		cur_id= pref.findFPrefFree(disp)

		cycle=append(cycle,findAgentFromId(prop,pref))
	}
	return cycle
}

func TTC(proposant []Agent, disposant []Agent) map[AgentID]AgentID{
  var couple = make(map[AgentID]AgentID)
  var unadmitted []Agent

  for _,v := range proposant {
			unadmitted=append(unadmitted,v)
	}

  for len(unadmitted) > 0 {
		cycle := BuildCycle(unadmitted,proposant,disposant)
		for len(cycle)!=0 && couple[cycle[len(cycle)-1].ID]=="" {
			couple[cycle[len(cycle)-1].ID]=cycle[len(cycle)-2].ID
			unadmitted=DeleteAgent(unadmitted, cycle[len(cycle)-2].ID)
			cycle=cycle[:len(cycle)-2]
		}
	}
	return couple
}*/


/*func ttc(proposant []Agent, disposant []Agent) map[AgentID]AgentID{
	var unadmitted []Agent //list des proposant pas admis
	var couple = make(map[AgentID]AgentID)
	var level = 0

	//initialisation des non proposant admis TODO : faire fonction initUnadmitted
	for x:=0; x<len(proposant); x++{
		unadmitted= append(unadmitted, proposant[x])
	}
	//initialisation des disposant non admis TOD0 : faire fonction initUnadmitted

	//map des propositions
	var proposition = make(map[AgentID][]Agent)

	for len(unadmitted) > 0 {
    var exchange_cycle []Agent

		//ont prends la liste des proposition en fonction du level
		proposition = propositionRecu(disposant_unadmitted, unadmitted, level)
		for key, element := range proposition {
			keyID := findAgentFromId(disposant_unadmitted, key)
			Pref := findBestPref(keyID, element) //TODO:verifier qu'il existe
			couple[key]= Pref.ID
		}

		//MAJ du nouveau groupe des unadmitted TODO : faire fonction deleteElementFromSlice
		for _,prop := range couple{
			for i:=0; i < len(unadmitted);i++{
				if unadmitted[i].ID == prop {
					unadmitted = removeFromSlice(unadmitted, i)
					i--
				}
			}
		}

		//TODO : faire fonction deleteElementFromSlice
		level +=1
	}
	return couple
}*/
