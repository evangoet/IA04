package main

import (
	"fmt"
	"sort"
	"strings"
)

func IsPalindrome(word string) bool {
	var i int
	var flag bool
	for i = 0; i < len(word); i++ {
		if word[i] == word[len(word)-(1+i)] {
			flag = true
		} else {
			return false
		}

	}
	return flag
}

func Palindromes(words []string) (l []string) {
	var i int
	l = make([]string, len(words))
	for i = 0; i < len(words); i++ {
		if IsPalindrome(words[i]) {
			l = append(l, words[i])
		}
	}
	return l
}

func FootPrint(s string) (footprint string) {
	var i int
	var l []string
	for i = 0; i < len(s); i++ {
		l = append(l, string([]byte{s[i]}))
	}
	sort.Strings(l)
	footprint = strings.Join(l, "")
	return footprint
}

func FindAnagrams(footprint string, words []string) []string {
	var i int
	var anagram []string
	for i = 0; i < len(words); i++ {
		if FootPrint(words[i]) == footprint {
			anagram = append(anagram, words[i])
		}
	}
	return anagram
}

func Anagrams(words []string) (anagrams map[string][]string) {
	var i int
	var result map[string][]string
	result = make(map[string][]string)
	var footprint string
	for i = 0; i < len(words); i++ {
		var mot string
		mot = words[i]
		footprint = FootPrint(mot)
		result[footprint] = FindAnagrams(footprint, words)
	}
	return result
}

func main() {
	fmt.Println(IsPalindrome("RADAR"))
	fmt.Println(IsPalindrome("ENCEINTE"))
	var dict []string
	dict = []string{"AGENT", "CHIEN", "COLOC", "ETANG", "ELLE", "GEANT", "NICHE", "RADAR"}
	fmt.Println(Palindromes(dict))
	fmt.Println(FootPrint("AGENT"))
	fmt.Println(Anagrams(dict))
}
