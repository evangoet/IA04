package main

import(
 "fmt"
 "math/rand"
)

func Fill(s1 []int) {
	var i int
	for i =0 ; i< len(s1);i++ {
		s1[i] = rand.Intn(100)
	}
}

func Moyenne(s1 []int) int{
	var i int
	var sum int
	for i=0 ; i< len(s1); i++{
		sum += s1[i]
	}
	var moyenne int
	moyenne = sum / len(s1)
	return moyenne
}

func ValeursCentrales(s1 []int) []int{
	var valeurs []int
	if len(s1)%2 == 0 {
		 valeurs = []int{s1[len(s1)/2]}
	}else{
		 valeurs = []int{s1[len(s1)/2],s1[(len(s1)/2)+1]}
	}
	return valeurs
}

func Plus1(s1 []int){
	var i int
	for i=0 ; i< len(s1); i++ {
		s1[i] +=1
	}
}

func main() {
	var slice = make([]int,10)
	fmt.Println("Contenu tableau originale :",slice)
	Fill(slice)
	fmt.Println("Contenu tableau apres fonction slice :",slice)
	var sum int
	sum = Moyenne(slice)
	fmt.Println("Moyenne du nouveau tableau :", sum)
	var valeurs []int
	valeurs = ValeursCentrales(slice)
	fmt.Println("Valeurs Moyennes du tableau :",valeurs)
	Plus1(slice)
	fmt.Println("Tableau en incrémentant de un chaque élement :", slice)
}
