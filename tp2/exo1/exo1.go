package main

import "fmt"

func compte(n int ){
    var i int
    for i=0; i<n ; i++{
      fmt.Println(i)
    }
}

func compteMsg(n int, msg string){
    var i int
    for i=0; i<n;i++{
      fmt.Println(msg)
      fmt.Println(i)
    }
}

func compteMsgFromTo(start int, end int, msg string){
  var i int
  for i=start; i<end;i++{
    fmt.Println(msg)
    fmt.Println(i)
  }
}

func main(){
    /*go compte(6)
    go compteMsg(10,"maman")
    go compteMsg(7,"papa")*/
    go compteMsgFromTo(0,10,"ma")
    go compteMsgFromTo(10,20,"ma")
    go compteMsgFromTo(20,30,"ma")
    go compteMsgFromTo(30,40,"ma")
    go compteMsgFromTo(40,50,"ma")
    go compteMsgFromTo(50,60,"ma")
    go compteMsgFromTo(60,70,"ma")
    go compteMsgFromTo(70,80,"ma")
    go compteMsgFromTo(80,90,"ma")
    go compteMsgFromTo(90,100,"ma")
    fmt.Scanln()
}
