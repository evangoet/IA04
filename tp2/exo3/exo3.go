package main

import (
    "fmt"
    "time"
)


 func bonneAnneeV1() {
    var i int
    for i=5; i>=0; i-- {
       fmt.Println(i)
       time.Sleep(time.Second)
     }
     fmt.Println("...")
     fmt.Println("bonne année")
 }

 func bonneAnneeV2(){
   var i int
   for i=5; i>=0;i-- {
     fmt.Println(i)
     c:= time.After(time.Second)
     <- c
   }
 }

 func bonneAnneeV3(){
   var i int
   for i=5; i>=0;i-- {
     fmt.Println(i)
     c:= time.Tick(time.Second)
     <- c
   }
 }

func main() {
    bonneAnneeV3()
    // A REVOIR

}
