package main

import (
    "fmt";
    "sync"
)

var l sync.Mutex
var n = 0


func f() {
    l.Lock()
    n++
    l.Unlock()
}

func main() {
    for i := 0; i < 10000; i++ {
        go f()
    }

    fmt.Println("Appuyez sur entrée")
    fmt.Scanln()
    fmt.Println("n:", n)
}
