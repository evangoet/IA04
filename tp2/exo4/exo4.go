package main

import(
  "fmt";
  "time"
)

func Fill(tab []int, v int){
  start := time.Now()
  var i int
  for i=0;i<len(tab);i++ {
    tab[i]=v
  }
  elapsedTime := time.Since(start)

  fmt.Println("Total Time For Execution: " + elapsedTime.String())
}

func fillIntervalle(tab []int, start int, end int, v int){
    for i := start ; i< end; i++{
      tab[i]=v
    }
}

func FillSync(tab []int, v int){
  start := time.Now()

  for i := 0; i <256; i++{
    go fillIntervalle(tab,i*10000,i*10000+256,v)
  }
  elapsedTime := time.Since(start)

  fmt.Println("Total Time For Execution: " + elapsedTime.String())
}

func main(){
  var tab [10000*256]int
  fmt.Println(tab)
  slice := tab[0:len(tab)]
  Fill(slice, 5)
  fmt.Println(slice)
  FillSync(slice, 5)

  fmt.Println(slice)
}
