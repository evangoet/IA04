package cosmoc

type Alternative int
type Profile [][]Alternative
type Count map[Alternative]int

type AgentID string

type Agent struct {
	ID    AgentID
	Name  string
	Prefs []Alternative
}

type AgentI interface {
	Equal(ag AgentI) bool
	DeepEqual(ag AgentI) bool
	Clone() AgentI
	String() string
	Prefers(a Alternative, b Alternative)
}

/*func rank(alt Alternative, prefs []Alternative) int{
  for i,x := range len(prefs){
    if alt == x{
      return i
      }
  }
}*/

func maxCount(count Count) (bestAlts []Alternative) {
	max := -1
	var best Alternative
	for i, x := range count {
		if x > max {
			max = x
			best = i
			bestAlts = nil
			bestAlts = append(bestAlts, best)
		} else if x == max {
			bestAlts = append(bestAlts, i)
		}
	}
	return bestAlts
}
