package cosmoc

func MajoritySWF(p Profile) (count Count, err error) {
	//fmt.Println(len(p))

	count = make(map[Alternative]int)
	//fmt.Println(count)

	for i := 0; i < len(p); i++ {
		//print(i)
		if _, ok := count[p[i][0]]; !ok {
			count[p[i][0]] = 1
		} else {
			count[p[i][0]] += 1
		}
	}
	//TODO : gérer les erreurs
	err = nil
	return count, err
}

func MajoritySCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := MajoritySWF(p)

	if err == nil {
		bestAlts = maxCount(count)
		return bestAlts, nil
	} else {
		return bestAlts, err
	}
}
