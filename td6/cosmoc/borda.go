package cosmoc

func BordaSWF(p Profile) (count Count, err error) {
	count = make(map[Alternative]int)
	//fmt.Println(count)

	for i := 0; i < len(p); i++ {
		//print(i)
		for y := 0; y < len(p[i]); y++ {
			if _, ok := count[p[i][y]]; !ok {
				count[p[i][y]] = len(p[i]) - y
			} else {
				count[p[i][y]] += len(p[i]) - y
			}
		}
	}
	err = nil
	return count, err
}

func BordaSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := BordaSWF(p)

	if err == nil {
		bestAlts = maxCount(count)
		return bestAlts, nil
	} else {
		return bestAlts, err
	}
}
