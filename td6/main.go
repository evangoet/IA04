package main

import (
	"IA04/td6/cosmoc"
	"fmt"
)

func main() {
	prefs := [][]cosmoc.Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
		{2, 1, 2},
	}
	/*count := map[cosmoc.Alternative]int{
		1: 3,
		2: 10,
		3: 5,
	}
	yo := cosmoc.maxCount(count)
	print(yo)*/
	//fmt.Println(len(prefs))
	// test MajoritySWF
	res, _ := cosmoc.MajoritySWF(prefs)
	fmt.Println(res)

	//test MajoritySCF
	res1, _ := cosmoc.MajoritySCF(prefs)
	fmt.Println(res1)

	//test BordaSWF
	res2, _ := cosmoc.BordaSWF(prefs)
	fmt.Println(res2)

	//test BordaSCF
	res3, _ := cosmoc.BordaSCF(prefs)
	fmt.Println(res3)

	//test ApprovalSWF
	res4, _ := cosmoc.ApprovalSWF(prefs)
	fmt.Println(res4)

	//test ApprovalSCF
	res5, _ := cosmoc.ApprovalSCF(prefs)
	fmt.Println(res5)
}
